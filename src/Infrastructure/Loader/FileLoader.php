<?php

namespace App\Infrastructure\Loader;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class FileLoader
{
    /**
     * @param string $fileName
     * @return array
     *
     * @throws \Exception
     */
    public function loadCsv(string $fileName): array
    {
        $fileName = \sprintf('%s/../../../data/SmsContent/%s', __DIR__, $fileName);

        $contents = [];
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $contents[] = $data;
            }
        } else {
            throw new \Exception(\sprintf('File not found: %s', $fileName));
        }

        return $contents;
    }
}