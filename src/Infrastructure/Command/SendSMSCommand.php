<?php
namespace App\Infrastructure\Command;

use App\Domain\Command\SendSmsCommand as SmsCommand;
use App\Domain\DTO\Content\ContentRequest;
use App\Domain\Handler\SendSmsHandler;
use App\Domain\Provider\SmsContentProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SendSMSCommand extends Command
{
    protected function configure()
    {
        $this->setName('send_valentine_sms');
        $this->addArgument('recipients', InputArgument::IS_ARRAY, 'Recipient number');
        $this->addOption('api', 'a', InputOption::VALUE_OPTIONAL, 'Chosen API', 'none');
        $this->addOption('api_token', 't', InputOption::VALUE_OPTIONAL, 'Api token', '');
        $this->addOption('content_file', 'f', InputOption::VALUE_OPTIONAL, 'Filename of content', 'default.sms');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $recipients = $input->getArgument('recipients');
        $token = $input->getOption('api_token');
        $api = $input->getOption('api');
        $filename = $input->getOption('content_file');

        if(empty($recipients)) {
            throw new \InvalidArgumentException('No recepients provided!');
        }

        $output->writeln(\sprintf('Sending message with - [%s]', $api));

        $provider = new SmsContentProvider();
        $contentRequest = new ContentRequest($filename);
        $content =  $provider->provideContent($contentRequest);

        $sendCommand = new SmsCommand($recipients, $content, $api, $token);
        $sendHandler = new SendSmsHandler();
        $sendHandler->handle($sendCommand);

        $output->writeln('Sucessfully sended');
    }

}