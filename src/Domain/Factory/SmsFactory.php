<?php

namespace App\Domain\Factory;

use App\Domain\DTO\Sms;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SmsFactory
{
    /**
     * @param array $recipients
     * @param string $content
     *
     * @return array
     */
    public function create(array $recipients, string $content): array
    {
        $smsList = [];

        foreach ($recipients as $recipient) {
            $smsList[] = new Sms($recipient, $content);
        }

        return $smsList;
    }

}