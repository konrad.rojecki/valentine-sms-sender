<?php

namespace App\Domain\DTO;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class Sms
{
    /**
     * @var string
     */
    private $recipient;

    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $recipient
     * @param string $content
     */
    public function __construct(string $recipient, string $content)
    {
        $this->recipient = $recipient;
        $this->content = $content;
    }

}