<?php

namespace App\Domain\DTO\Content;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class ContentRequest
{
    /**
     * @var \DateTime
     */
    private $sendDate;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @param string $fileName
     * @param \DateTime|null $sendDate
     *
     * @throws \Exception
     */
    public function __construct(string $fileName, \DateTime $sendDate = null)
    {
        if (null === $sendDate) {
            $sendDate = new \DateTime();
        }
        $this->sendDate = $sendDate;
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }


    /**
     * @return \DateTime
     */
    public function getSendDate(): \DateTime
    {
        return $this->sendDate;
    }
}