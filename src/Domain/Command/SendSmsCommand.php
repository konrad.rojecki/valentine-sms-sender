<?php

namespace App\Domain\Command;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SendSmsCommand implements SendSmsCommandInterface
{
    /**
     * @var string[]
     */
    private $recipients;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $apiToken;

    /**
     * @var string
     */
    private $api;

    /**
     * @param array $recipients
     * @param string $content
     * @param string $api
     * @param string $apiToken
     */
    public function __construct(array $recipients, string $content, string $api, string $apiToken)
    {
        $this->recipients = $recipients;
        $this->content = $content;
        $this->apiToken = $apiToken;
        $this->api = $api;

        $this->validate();
    }

    /**
     * @return array
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    /**
     *
     */
    private function validate()
    {
        $this->recipients = array_unique($this->recipients);
    }

    /**
    * @return string
    */
    public function getApi(): string
    {
        return $this->api;
    }


}