<?php

namespace App\Domain\Command;

/**
 * @author krojecki <konrad@rojecki.it>
 */
interface SendSmsCommandInterface
{
    /**
     * @return array
     */
    public function getRecipients(): array;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return string
     */
    public function getApiToken(): string;

    /**
     * @return string
     */
    public function getApi(): string;

}