<?php

namespace App\Domain\Provider;

use App\Domain\DTO\Content\ContentRequest;
use App\Domain\Resolver\ContentDateResolver;
use App\Infrastructure\Loader\FileLoader;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SmsContentProvider
{
    /**
     * @param ContentRequest $contentRequest
     *
     * @return string
     *
     * @throws \Exception
     */
    public function provideContent(ContentRequest $contentRequest): string
    {
        $fileLoader = new FileLoader();
        $resolver = new ContentDateResolver();

        $contents = $fileLoader->loadCsv($contentRequest->getFileName());
        $resolvedContents = $resolver->resolve($contents, $contentRequest->getSendDate());

        $magicNumber = rand(0, count($resolvedContents)-1);

        return $resolvedContents[$magicNumber];
    }
}