<?php

namespace App\Domain\Resolver;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class ContentDateResolver
{
    private const DATE_CONDITION = 0;
    private const CONTENT = 1;

    /**
     * @param array $contents
     * @param \DateTime $dateTime
     *
     * @return array
     */
    public function resolve(array $contents, \DateTime $dateTime): array
    {
        $dateFormatted = $dateTime->format('d.m.Y');
        $validContents = [];
        foreach($contents as $content) {
            if(false === fnmatch($content[self::DATE_CONDITION], $dateFormatted)) {
                continue;
            }

            $validContents[] = $content[self::CONTENT];
        }

        return $validContents;
    }
}