<?php

namespace App\Domain\Sender;

use App\Domain\DTO\Sms;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;
use Smsapi\Client\SmsapiHttpClient;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SMSAPiSender implements SmsSenderInterface
{
    private $apiToken;

    /**
     * @param Sms[] $smses
     */
    public function send(array $smses): void
    {
        $smsToSend = [];
        foreach($smses as $sms) {
            $smsToSend[] = $this->createSmsForApi($sms);
        }

        $smsApiClient = (new SmsapiHttpClient())->smsapiPlService($this->apiToken);

        foreach ($smsToSend as $sms) {
            $smsApiClient->smsFeature()
                ->sendSms($sms);
        }
    }

    /**
     * @param array $auth
     */
    public function setAuth(array $auth): void
    {
        if (!isset($auth['apiToken'])) {
            throw new \InvalidArgumentException('Missing api token for SMS API');
        }

        $this->apiToken = $auth['apiToken'];
    }

    /**
     * @param Sms $sms
     * @return SendSmsBag
     */
    private function createSmsForApi(Sms $sms): SendSmsBag
    {
        $preparedSms = SendSmsBag::withMessage($sms->getRecipient(), $sms->getContent());
        $preparedSms->encoding = 'utf-8';

        return $preparedSms;
    }

}