<?php

namespace App\Domain\Sender;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class NullSender implements SmsSenderInterface
{
    /**
     * @param array $smses
     */
    public function send(array $smses): void
    {
        var_dump($smses);
    }

    /**
     * @param array $auth
     */
    public function setAuth(array $auth): void
    {
        // do nothing
    }
}