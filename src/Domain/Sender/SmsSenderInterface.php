<?php

namespace App\Domain\Sender;

use App\Domain\DTO\Sms;

/**
 * @author krojecki <konrad@rojecki.it>
 */
interface SmsSenderInterface
{
    /**
     * @param Sms[] $smses
     */
    public function send(array $smses): void;

    /**
     * @param array $auth
     * @return mixed
     */
    public function setAuth(array $auth): void;
}