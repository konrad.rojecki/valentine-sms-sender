<?php

namespace App\Domain\Handler;

use App\Domain\Command\SendSmsCommandInterface;
use App\Domain\Enum\ApiEnum;
use App\Domain\Factory\SmsFactory;
use App\Domain\Sender\NullSender;
use App\Domain\Sender\SMSAPiSender;
use App\Domain\Sender\SmsSenderInterface;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class SendSmsHandler
{
    /**
     * @param SendSmsCommandInterface $sendSmsCommand
     */
    public function handle(SendSmsCommandInterface $sendSmsCommand): void
    {
        $sender = $this->getSender($sendSmsCommand->getApi());
        $sender->setAuth(['apiToken' => $sendSmsCommand->getApiToken()]);

        $factory = new SmsFactory();
        $smsList = $factory->create($sendSmsCommand->getRecipients(), $sendSmsCommand->getContent());
        $sender->send($smsList);
    }

    /**
     * @param string $api
     * @return SmsSenderInterface
     */
    private function getSender(string $api): SmsSenderInterface
    {
        switch ($api) {
            case ApiEnum::SMSAPI:
                return new SMSAPiSender();
            default:
                return new NullSender();
        }
    }
}