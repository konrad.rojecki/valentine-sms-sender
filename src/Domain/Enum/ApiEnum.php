<?php

declare(strict_types=1);

namespace App\Domain\Enum;

/**
 * @author krojecki <konrad@rojecki.it>
 */
class ApiEnum
{
    public const NONE = 'none';
    public const SMSAPI = 'smsapi';
}